FROM nikolaik/python-nodejs:python3.10-nodejs16-bullseye

# Environment variables
ENV DB_URL="postgres://user:pass@host/dbname"
ENV FS_URL="osfs:///data/360app"
ENV SERVER_NAME="localhost:5000"
ENV SECRET_KEY="see_readme"
ENV ENABLE_BLUR="true"
ENV BACKEND_MODE="api"

# Create folders
RUN mkdir -p /opt/360app/server /opt/360app/viewer /data/360app

# Install Python dependencies
WORKDIR /opt/360app/server
COPY ./server/requirements.txt ./
RUN pip install -r ./requirements.txt --no-cache-dir && \
    pip install waitress --no-cache-dir

# Install NodeJS dependencies
WORKDIR /opt/360app/viewer
COPY ./viewer/package*.json ./
RUN npm install --legacy-peer-deps

# Build viewer
COPY ./viewer ./
RUN npm run build

# Add server source files
WORKDIR /opt/360app
COPY ./docker-entrypoint.sh ./
RUN chmod +x ./docker-entrypoint.sh
COPY ./server ./server/
COPY ./images ./images/

# Expose service
EXPOSE 5000
ENTRYPOINT ["./docker-entrypoint.sh"]
