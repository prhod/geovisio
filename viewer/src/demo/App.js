import './css/App.css';
import Viewer from './../lib';

class App {
  constructor(){
    let instance = new Viewer(
      "viewer",
      //"https://360-dev.pavie.info/api/search",
      "http://localhost:5000/api/search",
      {
        position: [48.555570139,3.303271741],
        map: {
          startWide: false
        }
      }
    );

    instance.on("picture-loaded", (e, data) => {
      console.log("Switching to picture", data.picId, "at position lat", data.lat, "lon", data.lon, "heading to", data.x, "degrees");
    });

    instance.on("picture-tiles-loaded", (e, data) => {
      console.log("All tiles loaded for picture", data.picId);
    });

    instance.on("view-rotated", (e, position) => {
      console.log("New heading", position.x);
    });

//     instance.goToPosition(49.01008, 2.560299);
  }
}

export default App;
