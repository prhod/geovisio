import { AbstractButton, registerButton, DEFAULTS } from 'photo-sphere-viewer';

/**
 * @summary Navigation bar previous picture in sequence button class
 * @augments {external:photo-sphere-viewer.buttons.AbstractButton}
 * @memberof {external:photo-sphere-viewer.buttons}
 * @private
 */
class PreviousButton extends AbstractButton {

  static id = 'sequence-prev';
  static icon = '<svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"> <path d="m 33.823272,53.93303 24.455565,24.455565 c 2.148533,2.14854 5.68704,2.14854 7.835573,0 2.14854,-2.148533 2.14854,-5.68704 0,-7.835572 L 45.640482,50.015045 66.177125,29.478403 c 2.14854,-2.14854 2.14854,-5.68704 0,-7.835581 C 65.102885,20.568582 63.649775,20 62.25914,20 c -1.390103,0 -2.843722,0.56865 -3.917993,1.642822 L 33.822875,46.098387 c -2.14854,2.14854 -2.14854,5.68704 0,7.83558 z" fill="currentColor" /> </svg> ';

  /**
   * @param {external:photo-sphere-viewer.components.Navbar} navbar The navigation bar
   */
  constructor(navbar) {
    super(navbar, 'psv-button--hover-scale psv-sequence-button');
  }

  /**
   * @override
   * @description Goes to previous picture in sequence
   */
  onClick() {
    try {
      this.psv.goToPrevPicture();
    } catch(e) {
      console.warn(e);
      this.disable();
    }
  }

}

DEFAULTS.lang[PreviousButton.id] = 'Previous picture in sequence';
registerButton(PreviousButton, 'caption:left');
export default PreviousButton;
