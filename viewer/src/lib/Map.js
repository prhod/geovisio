import './css/Map.css';
import 'maplibre-gl/dist/maplibre-gl.css';
//~ import maplibregl from 'maplibre-gl';
// eslint-disable-next-line import/no-webpack-loader-syntax
import maplibregl from '!maplibre-gl';
import maplibreglWorker from 'maplibre-gl/dist/maplibre-gl-csp-worker';
import MiniComponentButtons from './MiniComponentButtons';
import MarkerSVG from './img/marker.svg';

maplibregl.workerClass = maplibreglWorker;

/**
 * @summary Map showing photo location
 * @private
 */
export default class Map {
	/**
	 * @param {external:photo-sphere-viewer.Viewer} psv The viewer
	 * @param {object} [options] Optional settings (can be any of [MapLibre GL settings](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters))
	 */
	constructor(psv, options = {}) {
		this.psv = psv;
		this.container = document.createElement("div");
		this.container.classList.add("psv-map", "gvs-map-small");
		this.psv.parent.appendChild(this.container);

		// Create map
		this._mapContainer = document.createElement("div");
		this._map = new maplibregl.Map({
			container: this._mapContainer,
			style: 'https://tile-vect.openstreetmap.fr/styles/basic/style.json',
			center: [0,0],
			zoom: 0,
			...options
		});
		this._map.addControl(new maplibregl.NavigationControl(), "top-right");

		// Widgets and markers
		this._miniButtons = new MiniComponentButtons(this);
		this.container.appendChild(this._mapContainer);
		this._picMarker = this._getPictureMarker();

		this._map.on("load", () => {
			this._createPicturesTilesLayer();
			this._listenToViewerEvents();
			this._map.resize();
		});
	}

	/**
	 * Change map rendering between small or wide
	 *
	 * @param {boolean} isWide True to make wide
	 */
	setWide(isWide) {
		if(isWide) {
			this.container.classList.remove("gvs-map-small");
		}
		else {
			this.container.classList.add("gvs-map-small");
		}
		this._map.resize();
	}

	/**
	 * Reduce component visibility (shown as a badge button)
	 *
	 * @private
	 */
	_minimize() {
		this.container.classList.add("gvs-map-minimized");
	}

	/**
	 * Show component as a classic widget (invert operation of minimize)
	 *
	 * @private
	 */
	_maximize() {
		this.container.classList.remove("gvs-map-minimized");
	}

	/**
	 * Create pictures/sequences vector tiles layer
	 *
	 * @private
	 */
	_createPicturesTilesLayer() {
		this._map.addSource('geovisio', {
			'type': 'vector',
			'tiles': [ this.psv._myApi.getPicturesTilesUrl() ],
			'minzoom': 0,
			'maxzoom': 14
		});

		this._map.addLayer({
			'id': 'pictures',
			'type': 'circle',
			'source': 'geovisio',
			'source-layer': 'pictures',
			...this._getPicturesLayerStyleProperties()
		});

		// Map interaction events (pointer cursor, click)
		this._picPopup = new maplibregl.Popup({ closeButton: false, closeOnClick: false });
		this._cachePicThumbUrls = {};

		this._map.on('mouseenter', 'pictures', e => {
			this._map.getCanvas().style.cursor = 'pointer';
			this._attachPreviewToPictures(e);
		});

		this._map.on('mouseleave', 'pictures', () => {
			this._map.getCanvas().style.cursor = '';
			this._picPopup.remove();
		});

		this._map.on('click', 'pictures', e => {
			this.psv.goToPicture(e.features[0].properties.id);
		});
	}

	/**
	 * MapLibre paint/layout properties for pictures layer
	 * This is useful when selected picture changes to allow partial update
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getPicturesLayerStyleProperties() {
		return {
			'paint': {
				'circle-radius': 6,
				'circle-color': '#0D47A1'
			},
			'layout': {}
		};
	}

	/**
	 * Attach a preview popup to a single picture.
	 * This is a mouseenter over pictures event handler
	 *
	 * @param {object} e Event data
	 * @private
	 */
	_attachPreviewToPictures(e) {
		let f = e.features[0];
		let picId = f.properties.id;
		let coordinates = f.geometry.coordinates.slice();
		while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
			coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
		}

		// Display thumbnail
		if(this._cachePicThumbUrls[picId]) {
			this._picPopup
			.setLngLat(coordinates)
			.setHTML(`<img class="gvs-map-thumb" src="${this._cachePicThumbUrls[picId]}" alt="Thumbnail of hovered picture" />`)
			.addTo(this._map);
		}
		else {
			this._picPopup
			.setLngLat(coordinates)
			.setHTML(`<i>Loading</i>`)
			.addTo(this._map);

			this._picPopup._loading = picId;

			this.psv._myApi.getPictureThumbnailUrl(f.properties.id)
			.then(thumbUrl => {
				if(this._picPopup._loading === picId) {
					this._cachePicThumbUrls[picId] = thumbUrl;
					delete this._picPopup._loading;

					if(thumbUrl) {
						this._picPopup.setHTML(`<img class="gvs-map-thumb" src="${thumbUrl}" alt="Thumbnail of hovered picture" />`)
					}
					else {
						this._picPopup.setHTML(`<i>No thumbnail</i>`);
					}
				}
			});
		}
	}

	/**
	 * Create a ready-to-use picture marker
	 *
	 * @returns {maplibregl.Marker} The generated marker
	 * @private
	 */
	_getPictureMarker() {
		const img = document.createElement("img");
		img.src = MarkerSVG;
		return new maplibregl.Marker({
			element: img
		});
	}

	/**
	 * Start listening to picture changes in PSV
	 *
	 * @private
	 */
	_listenToViewerEvents() {
		// Switched picture
		this.psv.on("picture-loaded", (e, d) => {
			// Show marker corresponding to selection
			this._picMarker
				.setLngLat([d.lon, d.lat])
				.setRotation(d.x)
				.addTo(this._map);

			// Move map to picture coordinates
			this._map.flyTo({
				center: [d.lon, d.lat],
				zoom: this._map.getZoom() < 15 ? 20 : this._map.getZoom(),
				maxDuration: 2000
			});
		});

		// Picture view rotated
		this.psv.on("view-rotated", (e, d) => {
			this._picMarker.setRotation(d.x);
		});
	}
}
