#!/bin/bash

usage() {
    echo "./docker-entrypoint.sh <COMMAND>: "
    echo -e "\tThis script simplifies running GeoVisio backend in a certain mode"
    echo "Commands: "
    echo -e "\tapi: Starts web API for production on port 5000 by default"
    echo -e "\tdev-api: Starts web API for development on port 5000 by default"
    echo -e "\tprocess-sequences: Starts analyzing pictures metadata"
    echo -e "\tcleanup: Cleans database and remove Geovisio derivated files (it doesn't delete your original pictures)"
}

# Check if BACKEND_MODE env var exists if no explicit argument given
if [ "${1}" == "" ]; then
    command=${BACKEND_MODE}
else
    command=${1}
fi

cwd=$(realpath $(dirname "${BASH_SOURCE[0]}"))
cd "$cwd/server"

echo "Executing \"${command}\" command"

case $command in
"api")
    python3 -m waitress --port 5000 --url-scheme=https --call 'src:create_app'
    ;;
"dev-api")
    FLASK_APP="src" FLASK_ENV=development python3 -m flask run
    ;;
"process-sequences")
    FLASK_APP="src" python3 -m flask process-sequences
    ;;
"cleanup")
    FLASK_APP="src" python3 -m flask cleanup
    ;;
*)
    usage
    ;;
esac

cd "$cwd"
