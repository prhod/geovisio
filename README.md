# GeoVisio

![GeoVisio logo](./images/logo_full.png)

GeoVisio is a complete solution for storing and __serving your own 360° geolocated pictures__ (like [StreetView](https://www.google.com/streetview/) / [Mapillary](https://mapillary.com/)).

Give it a try at [geovisio.fr](https://geovisio.fr/viewer) !

![Picture viewer screenshot](./images/screenshot.jpg)

## Features

* A __web viewer__ for 360° pictures
  * Move, zoom, drag pictures
  * Travel between pictures in sequences
  * Find pictures by location with a mini-map
* A __web API__ to search / query the pictures collection
  * Search pictures by ID, date, location
  * Compatible with [SpatioTemporal Asset Catalog](https://stacspec.org/) and [OGC WFS 3](https://github.com/opengeospatial/WFS_FES) specifications
* An easy-to-use __backend__
  * Reads through your pictures collection and creates metadata in database
  * Generates automatically thumbnail, small and tiled versions of your pictures
  * Offers blurring of people and vehicle to respect privacy laws
  * Serves a default website and viewer


## Usage

### With Docker

The [Docker](https://docs.docker.com/get-docker/) deployment offers both server and viewer in a single image, and is the simplest way to get a GeoVisio up and running.

```bash
# Create the "pictures" folder
mkdir ./pictures

# Add your sequences in pictures folder
# Each sequence should be a folder containing JPEG images
mv /my/first/sequence ./pictures/first_sequence
mv /my/other/sequence ./pictures/sequence_two
...

# Start GeoVisio API
docker-compose -f docker-compose.yml up

# Launch processing of your pictures
docker compose -f docker-compose.yml run backend process-sequences
```

Then, you can access to your instance at [localhost:5000](http://localhost:5000).

### Manual install

Complete installation and usage procedure is described for each component of GeoVisio:

- [Server](./server/), the component handling hosting of pictures. It analyzes and serves your pictures through an API.
- [Viewer](./viewer/), the component allowing display of pictures and sequences on a web page. It is written in JavaScript and compatible with all modern web browsers

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate. See also [develop doc](DEVELOP.md) for more information.

## License

Copyright (c) Adrien Pavie 2022, [released under MIT license](./LICENSE).

Development of this tool was made possible thanks to [GéoVélo](https://geovelo.fr/) and [Carto Cité](https://cartocite.fr/) funding.

![Sponsors](./images/sponsors.png)
