--
-- Script for database cleanup
--

DROP TABLE IF EXISTS next_sequences CASCADE;
DROP TABLE IF EXISTS sequences_pictures CASCADE;
DROP TABLE IF EXISTS sequences CASCADE;
DROP TYPE IF EXISTS sequence_status;
DROP TABLE IF EXISTS pictures CASCADE;
DROP TYPE IF EXISTS picture_status;
