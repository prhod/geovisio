import os
import psycopg
import re
import configparser
from flask import Flask, g, jsonify, render_template, send_from_directory
from flask.cli import with_appcontext
from flask_cors import CORS
from flask_compress import Compress
from flasgger import Swagger
from fs import open_fs
from . import pictures, runner_pictures, errors, stac, map


def create_app(test_config=None):
	"""API launcher method"""

	#
	# Create and setup Flask App
	#

	app = Flask(__name__, instance_relative_config=True)
	CORS(app)
	Compress(app)

	# Load by default from config file
	app.config.from_pyfile(
		os.path.join(os.path.dirname(__file__), '../config.py'),
		silent=True
	)

	# Override eventually from environment variables
	confFromEnv = [ 'FS_URL', 'DB_URL', 'DB_PORT', 'DB_HOST', 'DB_USERNAME', 'DB_PASSWORD', 'DB_NAME', 'SECRET_KEY', 'SERVER_NAME', 'ENABLE_BLUR' ]
	for e in confFromEnv:
		if os.environ.get(e):
			app.config[e] = os.environ.get(e)

	# Create DB_URL from separated parameters
	if 'DB_PORT' in app.config or 'DB_HOST' in app.config or 'DB_USERNAME' in app.config or 'DB_PASSWORD' in app.config:
		user = app.config['DB_USERNAME'] if 'DB_USERNAME' in app.config else ""
		passw = app.config['DB_PASSWORD'] if 'DB_PASSWORD' in app.config else ""
		host = app.config['DB_HOST'] if 'DB_HOST' in app.config else ""
		port = app.config['DB_PORT'] if 'DB_PORT' in app.config else ""
		dbname = app.config['DB_NAME'] if 'DB_NAME' in app.config else ""

		app.config['DB_URL'] = f"postgres://{user}:{passw}@{host}:{port}/{dbname}"

	# Final overriding from test_config
	if test_config is not None:
		app.config.update(test_config)

	# Add generated config vars
	if 'ENABLE_BLUR' in app.config and app.config['ENABLE_BLUR'] in [True, "true", "1", 1, "yes", "y", "True"]:
		app.config['ENABLE_BLUR'] = 1
	elif 'ENABLE_BLUR' in app.config and app.config['ENABLE_BLUR'] in [False, "false", "0", 0, "no", "n", "False"]:
		app.config['ENABLE_BLUR'] = 0
	if 'ENABLE_BLUR' not in app.config:
		app.config['ENABLE_BLUR'] = 1
	app.config['ENABLE_BLUR'] = int(app.config['ENABLE_BLUR'])

	app.url_map.strict_slashes = False
	app.config['PIC_DERIVATES_DIR'] = "/gvs_derivates"
	app.config['COMPRESS_MIMETYPES'].append("application/geo+json")

	# Prepare filesystem
	createDirNoFailure(app.instance_path)
	pic_fs = open_fs(app.config['FS_URL'])
	pic_fs.makedirs(app.config['PIC_DERIVATES_DIR'], recreate=True)


	#
	# API documentation
	#

	# Read API metadata from setup.cfg
	setupCfg = configparser.RawConfigParser()
	setupCfg.read(os.path.join(os.path.dirname(__file__), '../setup.cfg'))
	apiMeta = dict(setupCfg.items('metadata'))

	apiDocs = {
		"info": {
			"title": apiMeta['name'],
			"version": apiMeta['version'],
			"description": apiMeta['description'],
			"contact": {
				"name": apiMeta['maintainer'],
				"url": apiMeta['url'],
				"email": apiMeta['maintainer_email']
			}
		},
		"schemes": [
			"http",
			"https"
		],
		"definitions": {
			"Collections": {
				"type": "object",
				"required": [ "links", "collections" ],
				"properties": {
					"links": {"type": "array", "items": { "$ref": "#/definitions/Link" } },
					"collections": {"type": "array", "items": { "$ref": "https://schemas.stacspec.org/v1.0.0/collection-spec/json-schema/collection.json" } }
				}
			},
			"Link": {
				"type": "object",
				"required": ["href", "rel"],
				"properties": {
					"href": { "type": "string", "example": "http://data.example.com/buildings/123" },
					"rel": { "type": "string", "example": "alternate" },
					"type": { "type": "string", "example": "application/geo+json" },
					"title": { "type": "string", "example": "Trierer Strasse 70, 53115 Bonn" }
				}
			}
		}
	}
	swagger = Swagger(app, template=apiDocs)


	#
	# List available routes/blueprints
	#

	app.register_blueprint(pictures.bp)
	app.register_blueprint(stac.bp)
	app.register_blueprint(map.bp)

	# Main page
	@app.route('/')
	def index():
		return render_template('main.html')

	# Viewer
	@app.route('/viewer')
	def viewer():
		return render_template('viewer.html')

	@app.route('/static/media/<path:path>')
	def viewer_static(path):
		return send_from_directory(
			os.path.join(os.path.dirname(__file__), '../../viewer/build/static/media'),
			path
		)

	@app.route('/static/img/<path:path>')
	def viewer_img(path):
		return send_from_directory(
			os.path.join(os.path.dirname(__file__), '../../images'),
			path
		)

	@app.route('/viewer/lib/<path:path>')
	def viewer_lib(path):
		return send_from_directory(
			os.path.join(os.path.dirname(__file__), '../../viewer/build'),
			path
		)

	# Errors
	@app.errorhandler(errors.InvalidAPIUsage)
	def invalid_api_usage(e):
		return jsonify(e.to_dict()), e.status_code


	#
	# Add CLI functions
	#

	@app.cli.command("process-sequences")
	@with_appcontext
	def process_sequences():
		runner_pictures.run()

	@app.cli.command("cleanup")
	@with_appcontext
	def cleanup():
		runner_pictures.cleanup()


	# Check database connection
	if 'DB_URL' in app.config:
		with psycopg.connect(app.config['DB_URL']) as conn:
			with conn.cursor() as cursor:
				# Check if DB has its structure initialized
				picturesTableExists = cursor.execute("SELECT EXISTS(SELECT relname FROM pg_class WHERE relname = 'pictures')").fetchone()[0]

				# Initialize DB
				if not picturesTableExists:
					print("Database seems empty, initializing...")
					sqlSetupLines = open(os.path.join(os.path.dirname(__file__), './db_setup.sql'), "r").readlines()
					sqlSetupLines = " ".join([
						re.sub("--.*", "", l.replace("\n", " "))
						for l in sqlSetupLines
					])
					conn.execute(sqlSetupLines)
					conn.commit()
					conn.close()
					print("Database initialized")

				return app
	else:
		return app


def createDirNoFailure(directory):
	"""Creates a directory on disk if not already existing

	Parameters
	----------
	directory : str
		Path of the directory to create
	"""

	try:
		os.makedirs(directory)
	except OSError:
		pass
