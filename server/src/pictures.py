import psycopg
from psycopg.rows import dict_row
import re
import io
from fs import open_fs
import math
import time
from flask import Blueprint, current_app, request, send_from_directory, send_file
from PIL import Image
from . import errors

bp = Blueprint('pictures', __name__, url_prefix='/api/pictures')


def getPictureSizing(picture):
	"""Calculates image dimensions (width, height, amount of columns and rows for tiles)

	Parameters
	----------
	picture : PIL.Image
		Picture

	Returns
	-------
	dict
		{ width, height, cols, rows }
	"""
	tileSize = getTileSize(picture.size)
	return { "width": picture.size[0], "height": picture.size[1], "cols": tileSize[0], "rows": tileSize[1] }


def getDerivatesPath(pictureId):
	"""Get the path to GeoVisio picture derivates version as a string"""
	return f"{current_app.config['PIC_DERIVATES_DIR']}/{str(pictureId)[0:2]}/{pictureId}"


def checkPictureStatus(pictureId):
	"""Checks if picture exists in database, is ready to serve, and retrieves its metadata"""

	# Check picture availability + status
	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as db:
		picMetadata = db.execute("""
			SELECT
				status, file_path,
				(metadata->>'cols')::int AS cols,
				(metadata->>'rows')::int AS rows
			FROM pictures WHERE id = %s
		""", [pictureId]).fetchone()

		if picMetadata is not None:
			if picMetadata['status'] == "ready":
				return picMetadata
			else:
				raise errors.InvalidAPIUsage("Picture is not available (either hidden by admin or processing)", status_code=403)
		else:
			raise errors.InvalidAPIUsage("Picture can't be found, you may check its ID", status_code=404)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/<uuid:pictureId>/hd.jpg')
def getPictureHD(pictureId):
	"""Get picture image (high-definition JPEG)
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
		responses:
			200:
				description: High-definition JPEG
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
	"""

	picPath = checkPictureStatus(pictureId)['file_path']

	with open_fs(current_app.config['FS_URL']) as fs:
		# Check if a blurred version exists
		blurPicPath = getDerivatesPath(pictureId) + "/blurred.jpg"

		try:
			if fs.isfile(blurPicPath):
				return send_file(fs.openbin(blurPicPath), mimetype='image/jpeg')
			else:
				return send_file(fs.openbin(picPath), mimetype='image/jpeg')
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)


def createSDPicture(fs, picture, outputFilename):
	"""Create a standard definition version of given picture and save it on filesystem

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	outputFilename : str
		Path to output file (relative to instance root)

	Returns
	-------
	bool
		True if operation was successful
	"""

	sdImg = picture.resize((2048, int(picture.size[1]*2048/picture.size[0])))

	sdImgBytes = io.BytesIO()
	sdImg.save(sdImgBytes, format="jpeg", quality=90)
	fs.writebytes(outputFilename, sdImgBytes.getvalue())

	return True


@bp.route('/<uuid:pictureId>/sd.jpg')
def getPictureSD(pictureId):
	"""Get picture image (standard definition JPEG)
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
		responses:
			200:
				description: Standard definition JPEG (width of 2048px)
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
	"""

	checkPictureStatus(pictureId)

	with open_fs(current_app.config['FS_URL']) as fs:
		picPath = getDerivatesPath(pictureId) + "/sd.jpg"
		try:
			return send_file(fs.openbin(picPath), mimetype='image/jpeg')
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)


def createThumbPicture(fs, picture, outputFilename):
	"""Create a thumbnail version of given picture and save it on filesystem

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	outputFilename : str
		Path to output file (relative to instance root)

	Returns
	-------
	bool
		True if operation was successful
	"""

	tbImg = picture.resize((2000, 1000)).crop((750, 350, 1250, 650))

	tbImgBytes = io.BytesIO()
	tbImg.save(tbImgBytes, format="jpeg", quality=90)
	fs.writebytes(outputFilename, tbImgBytes.getvalue())

	return True


@bp.route('/<uuid:pictureId>/thumb.jpg')
def getPictureThumb(pictureId):
	"""Get picture thumbnail
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
		responses:
			200:
				description: 500px wide ready-for-display JPEG
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
	"""

	checkPictureStatus(pictureId)

	with open_fs(current_app.config['FS_URL']) as fs:
		picPath = getDerivatesPath(pictureId) + "/thumb.jpg"
		try:
			return send_file(fs.openbin(picPath), mimetype='image/jpeg')
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)


def getTileSize(imgSize):
	"""Compute ideal amount of rows and columns to give a tiled version of an image according to its original size

	Parameters
	----------
	imgSize : tuple
		Original image size, as (width, height)

	Returns
	-------
	tuple
		Ideal tile splitting as (cols, rows)
	"""

	possibleCols = [4,8,16,32,64] # Limitation of PSV, see https://photo-sphere-viewer.js.org/guide/adapters/tiles.html#cols-required
	idealCols = max(min(int(int(imgSize[0] / 512) / 2) * 2, 64), 4)
	cols = possibleCols[0]
	for c in possibleCols:
		if idealCols >= c:
			cols = c
	return (int(cols), int(cols/2))


def createTiledPicture(fs, picture, destPath, cols, rows):
	"""Create tiled version of an input image into destination directory.

	Output images are named following col_row.jpg format, 0_0.jpg being the top-left corner.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	destPath : str
		Path of the output directory
	cols : int
		Amount of columns for splitted image
	rows : int
		Amount of rows for splitted image
	"""

	colWidth = math.floor(picture.size[0] / cols)
	rowHeight = math.floor(picture.size[1] / rows)

	for col in range(cols):
		for row in range(rows):
			tilePath = destPath + "/" + str(col) + "_" + str(row) + ".jpg"
			tile = picture.crop((colWidth * col, rowHeight * row, colWidth * (col+1), rowHeight * (row+1)))

			tileBytes = io.BytesIO()
			tile.save(tileBytes, format="jpeg", quality=90, subsampling=0)
			fs.writebytes(tilePath, tileBytes.getvalue())

	return True


@bp.route('/<uuid:pictureId>/tiled/<int:col>_<int:row>.jpg')
def getPictureTile(pictureId, col, row):
	"""Get picture tile
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
			- name: col
			  in: path
			  type: number
			  description: Tile column ID
			- name: row
			  in: path
			  type: number
			  description: Tile row ID
		responses:
			200:
				description: Tile JPEG (size depends of original image resolution, square with side size around 512px)
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
	"""

	metadata = checkPictureStatus(pictureId)

	if col < 0 or col >= metadata['cols']:
		raise errors.InvalidAPIUsage("Column parameter is invalid", status_code=404)
	if row < 0 or row >= metadata['rows']:
		raise errors.InvalidAPIUsage("Column parameter is invalid", status_code=404)

	with open_fs(current_app.config['FS_URL']) as fs:
		picPath = f"{getDerivatesPath(pictureId)}/tiles/{col}_{row}.jpg"
		try:
			return send_file(fs.openbin(picPath), mimetype='image/jpeg')
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)
