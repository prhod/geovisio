#!/usr/bin/env python3

"""
Blur persons in photo.

Person detection based on pretrained Deeplabv3 tensorflow model:
https://averdones.github.io/real-time-semantic-image-segmentation-with-deeplab-in-tensorflow/

Script based on https://github.com/Stefal/blur-persons
"""

import os
import io
import sys
import math
import glob
import urllib.request
import tarfile
import os.path
import argparse
import platform
import tempfile
import subprocess
import collections
import datetime
import shutil
from fs import open_fs
from pathlib import Path

import numpy as np

from PIL import Image, ImageDraw, ImageFilter, ImageColor

from packaging import version

try:
    import tensorflow.compat.v1 as tf
    if version.parse(tf.__version__) < version.parse('1.5'):
        raise ImportError('Please upgrade your tensorflow installation to v1.5.0 or newer!')
except:
    print('tensorflow.compat.v1 not available')


LABEL_NAMES = [
    'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
    'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog',
    'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa',
    'train', 'tv'
]

MODEL_NAME = "xception_coco_voctrainval"
MODEL_URL = 'http://download.tensorflow.org/models/deeplabv3_pascal_trainval_2018_01_04.tar.gz'

class DeepLabModel(object):
    """Class to load deeplab model and run inference."""

    FROZEN_GRAPH_NAME = 'frozen_inference_graph'
    INPUT_TENSOR_NAME = 'ImageTensor:0'
    OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
    INPUT_SIZE = 513

    def __init__(self, tarball_path):
        """Creates and loads pretrained deeplab model."""
        self.graph = tf.Graph()

        graph_def = None
        # Extract frozen graph from tar archive.
        tar_file = tarfile.open(tarball_path)
        for tar_info in tar_file.getmembers():
            if self.FROZEN_GRAPH_NAME in os.path.basename(tar_info.name):
                file_handle = tar_file.extractfile(tar_info)
                graph_def = tf.GraphDef.FromString(file_handle.read())
                break

        tar_file.close()

        if graph_def is None:
            raise RuntimeError('Cannot find inference graph in tar archive.')

        with self.graph.as_default():
            tf.import_graph_def(graph_def, name='')

        self.sess = tf.Session(graph=self.graph)

    def run(self, resized_image):
        """Runs inference on a single image.

        Args:
            image: A PIL.Image object, raw input image.

        Returns:
            resized_image: RGB image resized from original input image.
            segmentation_map: Segmentation map of `resized_image`.
        """
        batch_segmentation_map = self.sess.run(
            self.OUTPUT_TENSOR_NAME,
            feed_dict={self.INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
        segmentation_map = batch_segmentation_map[0]
        return resized_image, segmentation_map


def split_area(area_width, area_height, box_width, box_height, is_360=None, overlap_factor=0.15):
    """
    Yield a list of box (x1,y1,x2,y2)
    representing cut of the areas in
    overlapping smaller pieces.

    If is_360 is True, the returned last boxes will be larger than the area
    in order to wrap back to the left of the area.
    """
    overlap_factor = max(0.0, min(0.5, overlap_factor)) # ensure in range 0.0 … 0.5
    if is_360 is None:
        is_360 = (area_width == (2 * area_height))
    real_area_width = (area_width + box_width*overlap_factor) if is_360 else area_width
    nb_x = math.ceil(real_area_width / (box_width - overlap_factor*box_width)) if real_area_width > box_width else 1
    while (nb_x > 2) and ((box_width*(1.0-overlap_factor)*(nb_x-2) + box_width) > real_area_width):
        nb_x = nb_x - 1
    nb_y = math.ceil(area_height / (box_height - overlap_factor*box_height)) if area_height > box_height else 1
    while (nb_y > 2) and ((box_height*(1.0-overlap_factor)*(nb_y-2) + box_height) > area_height):
        nb_y = nb_y - 1
    if nb_x > 1:
        factor_width = (real_area_width - box_width) / (box_width * (nb_x - 1))
    else:
        factor_width = 0.0
    if nb_y > 1:
        factor_height = (area_height - box_height) / (box_height * (nb_y - 1))
    else:
        factor_height = 0.0
    for i in range(nb_x):
        x = int(i*box_width*factor_width)
        for j in range(nb_y):
            y = int(j*box_height*factor_height)
            yield(x, y, x+box_width, y+box_height)

def iter_image_sub_boxes(image_width, image_height, box_size, is_360=None, overlap_factor=0.15):
    box_width = min(box_size, image_width)
    box_height = min(box_size, image_height)
    for result in split_area(image_width, image_height, box_width, box_height, is_360, overlap_factor):
        yield result
    if min(image_width, image_height) >= 2*box_size:
        box_size = min(image_width, image_height)
        for result in split_area(image_width, image_height, box_size, box_size, is_360, overlap_factor):
            yield result

def blur_from_model_and_colormap(original_image, model, colormap, dezoom=1.0):
    blur = 30
    width, height = original_image.size
    is_360 = (width == (2 * height))
    if type(blur) is int:
        blurred_im = original_image.filter(ImageFilter.GaussianBlur(radius=blur))
    else:
        blurred_im = Image.new('RGB', original_image.size, color=blur)
    new_image = original_image.copy()
    for x1,y1,x2,y2 in iter_image_sub_boxes(width, height, int(model.INPUT_SIZE*dezoom)):
        extract_width = x2-x1
        extract_height = y2-y1
        if x2 >= width and is_360:
            # The (x1,y1,x2,y2) box wrap from the far right of the image back to
            # the left (360° image), so we need to take two boxes, one on the
            # right and one on the left:
            x2_1 = width
            x2_2 = x2 - width
            extract_width_1 = x2_1 - x1
            extract_width_2 = x2_2
            extract_image = Image.new('RGB', (extract_width, extract_height))
            extract_image.paste(original_image.crop((x1,y1, x2_1, y2)), (0,0))
            extract_image.paste(original_image.crop((0,y1, x2_2, y2)), (extract_width_1,0))
        else:
            extract_image = original_image.crop((x1,y1, x2,y2))

        extract_width, extract_height = extract_image.size
        resize_ratio = 1.0 * model.INPUT_SIZE / max(extract_width, extract_height)
        target_size = (int(resize_ratio * extract_width), int(resize_ratio * extract_height))
        resized_image = extract_image.convert('RGB').resize(target_size, Image.Resampling.LANCZOS)

        resized_im, segmentation_map = model.run(resized_image)
        segmentation_mask = Image.fromarray(np.uint8(colormap[segmentation_map])).resize((x2-x1, y2-y1), Image.Resampling.LANCZOS)
        if x2 >= width and is_360:
            new_image.paste(blurred_im.crop((x1,y1, x2_1,y2)), (x1,y1), segmentation_mask.crop((0,0, extract_width_1, extract_height)))
            new_image.paste(blurred_im.crop((0,y1, x2_2,y2)), (0,y1), segmentation_mask.crop((extract_width_1,0, extract_width, extract_height)))
        else:
            new_image.paste(blurred_im.crop((x1,y1, x2,y2)), (x1,y1), segmentation_mask)

    return new_image

tarball_name = os.path.basename(MODEL_URL)
model_dir = os.path.join(os.path.dirname(__file__), MODEL_NAME) # or tempfile.mkdtemp()
download_path = os.path.join(model_dir, tarball_name)

def download_model():
    if not os.path.exists(download_path):
        tf.gfile.MakeDirs(model_dir)
        urllib.request.urlretrieve(MODEL_URL, download_path + ".tmp")
        os.rename(download_path + ".tmp", download_path)

def prepare():
    """Prepare blur module

    Returns
    -------
    DeepLabModel
        Model to use for blurring
    """

    download_model()
    return DeepLabModel(download_path)

def blurPicture(fs, picture, outputFilepath, model, classes = ["bus","car","motorbike","person"], dezoom = 1.0):
    """Blurs a single picture and writes new version on filesystem

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Picture file
	outputFilepath : str
		Path to output file (relative to instance root)
    model : DeepLabModel
        Model to use for blur
    classes : list
        Labels to look for
    dezoom : float
        Factor for blur precision (1 by default, higher values = faster but less precise)

	Returns
	-------
	io.IOBase
		Blurred version of picture
	"""

    blur_colormap = np.zeros((512,4), dtype=int)

    for clazz in classes:
        index = LABEL_NAMES.index(clazz)
        blur_colormap[index] = (255,255,255,255)

    new_image = blur_from_model_and_colormap(picture, model, blur_colormap, dezoom)
    newImgBytes = io.BytesIO()
    new_image.save(newImgBytes, format="jpeg", quality=90, subsampling=0)
    fs.writebytes(outputFilepath, newImgBytes.getvalue())

    return newImgBytes
