import re
import datetime
from fs import open_fs
from PIL import Image, ExifTags
from flask import current_app
import psycopg
import traceback
import os
from psycopg.types.json import Jsonb
from collections import Counter
from src import pictures
from concurrent.futures import as_completed, ThreadPoolExecutor
from itertools import repeat

def run():
	"""Launch processing of pictures and sequences"""

	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL']) as db:
			seqFolders = listSequencesToProcess(fs, db)

			if len(seqFolders) > 0:
				print(len(seqFolders), "sequences to process")

				for seqFolder in seqFolders:
					processSequence(fs, db, seqFolder)

				print("Done processing", len(seqFolders), "sequences")
			else:
				print("No sequence to process for now")


def cleanup():
	"""Removes database tables and deletes Geovisio derivated versions of pictures"""

	print("Cleaning up database...")
	with psycopg.connect(current_app.config['DB_URL']) as conn:
		sqlLines = open(os.path.join(os.path.dirname(__file__), './db_cleanup.sql'), "r").readlines()
		sqlLines = " ".join([
			re.sub("--.*", "", l.replace("\n", " "))
			for l in sqlLines
		])
		conn.execute(sqlLines)
		conn.commit()
		conn.close()

		print("Deleting derivated files...")
		with open_fs(current_app.config['FS_URL']) as fs:
			fs.removetree(current_app.config['PIC_DERIVATES_DIR'])

			print("Cleanup done")


def listSequencesToProcess(fs, db):
	"""Find new sequences folder to process

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection

	Returns
	-------
	list
		List of sequences folders to import
	"""

	sequencesFolders = [ x for x in fs.listdir("/") if fs.isdir("/" + x) and not x.startswith("gvs_") ]
	sequencesInDb = db.execute("SELECT array_agg(folder_path) FROM sequences").fetchone()[0]
	diff = Counter(sequencesFolders) - Counter(sequencesInDb)
	return list(diff.elements())


def processSequence(fs, db, sequenceFolder):
	"""Processes a single sequence (list pictures, read metadata, populate database)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	"""

	# List available pictures in sequence folder
	picturesFilenames = sorted([
		f for f in fs.listdir(sequenceFolder)
		if re.search(r'\.jpe?g$', f, re.IGNORECASE)
	])

	if len(picturesFilenames) > 0:
		# Create sequence in database
		seqId = db.execute("INSERT INTO sequences(folder_path) VALUES(%s) RETURNING id", [sequenceFolder]).fetchone()[0]

		# Process pictures
		maxWorkers = min(2, os.cpu_count()) if current_app.config['ENABLE_BLUR'] >= 1 else os.cpu_count()
		print("Processing", len(picturesFilenames), "pictures in sequence", sequenceFolder, "using", maxWorkers, "workers")
		picProcessFailures = 0
		picIds = []
		model = None

		# Load blur model in-memory if necessary
		if current_app.config['ENABLE_BLUR'] >= 1:
			from src.blur import prepare as blur_prepare
			model = blur_prepare()

		with ThreadPoolExecutor(max_workers=maxWorkers) as executor:
			picProcResults = executor.map(processPicture, repeat(fs), repeat(db), repeat(sequenceFolder), picturesFilenames, repeat(current_app.config), repeat(model), repeat(True))

			for picProcRes in picProcResults:
				picIds.append(picProcRes['id'])
				if picProcRes['status'] == "broken":
					picProcessFailures += 1

			print("") # For line return after pictures dots display

		if picProcessFailures > 0:
			print(picProcessFailures, "pictures failed in sequence", sequenceFolder)

		if picProcessFailures == len(picturesFilenames):
			# Remove sequence if all its pictures failed
			db.execute("DELETE FROM sequences WHERE id = %s", [seqId])
			db.commit()
			print("Import of sequence", sequenceFolder,"is cancelled (all pictures failed)")
		else:
			# Update sequence metadata and status
			# Create link between pictures and sequence
			with db.cursor() as cursor:
				with cursor.copy("COPY sequences_pictures(seq_id, rank, pic_id) FROM STDIN") as copy:
					for i, p in enumerate(picIds):
						copy.write_row((seqId, i+1, p))

			db.execute("""
				UPDATE sequences
				SET status = 'ready', geom = ST_MakeLine(ARRAY(
					SELECT p.geom
					FROM sequences_pictures sp
					JOIN pictures p ON sp.pic_id = p.id
					WHERE sp.seq_id = %s
					ORDER BY sp.rank
				))
				WHERE id = %s
			""", (seqId, seqId))
			db.commit()
			print("Sequence", sequenceFolder, "is ready")
	else:
		print("Skipped empty sequence", sequenceFolder)


def processPicture(fs, db, sequenceFolder, pictureFilename, config, model = None, dots = False):
	"""Processes a single picture (read file, extract metadata, generate derivate files, insert in database)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)
	model : DeepLabModel (optional)
		The blur model

	Returns
	-------
	dict
		{ id, status }
	"""

	# Create a fully-featured metadata object
	picture = Image.open(fs.openbin(sequenceFolder + "/" + pictureFilename))
	metadata = readPictureMetadata(picture) | pictures.getPictureSizing(picture)

	# Add picture metadata to database (with "preparing" status)
	picId = db.execute("""
		INSERT INTO pictures (file_path, ts, heading, metadata, geom)
		VALUES (%s, to_timestamp(%s), %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326))
		RETURNING id
	""", (
		sequenceFolder + "/" + pictureFilename,
		metadata['ts'],
		metadata['heading'],
		Jsonb(metadata),
		metadata['lon'],
		metadata['lat']
	)).fetchone()[0]

	picProcessStatus = "preparing"

	# Generate derivated picture versions
	try:
		picDerivatesFolder = f"{config['PIC_DERIVATES_DIR']}/{str(picId)[0:2]}/{picId}"
		fs.makedirs(picDerivatesFolder, recreate=True)

		# Create blurred version if required
		if config['ENABLE_BLUR'] >= 1:
			if model is None:
				raise Exception("Blur model was not initialized, please call run() method in runner_pictures before trying to process pictures")
			from src.blur import blurPicture
			blurPicBytes = blurPicture(fs, picture, picDerivatesFolder + "/blurred.jpg", model, dezoom = config['ENABLE_BLUR'])
			picture = Image.open(blurPicBytes) # Swap original picture and blurred version

		# Create SD, thumbnail and tiles
		generatePictureDerivates(fs, picture, metadata, picDerivatesFolder)

		picProcessStatus = "ready"

		if dots:
			print(".", end='', flush=True)

	except Exception as e:
		picProcessStatus = "broken"
		print("---------------------------------------------------------")
		print("An error occured when processing picture", pictureFilename)
		print(''.join(traceback.format_exception(type(e), e, e.__traceback__)))
		print("---------------------------------------------------------")

	# Mark picture as ready in database
	finally:
		db.execute("UPDATE pictures SET status = %s WHERE id = %s", (picProcessStatus, picId))
		return { "id": picId, "status": picProcessStatus }


def readPictureMetadata(picture):
	"""Extracts metadata from picture file

	Parameters
	----------
	picture : PIL.Image
		Picture file

	Returns
	-------
	dict
		Various metadata fields : lat, lon, ts, heading
	"""

	data = {}

	info = picture._getexif()
	if info:
		for tag, value in info.items():
			decoded = ExifTags.TAGS.get(tag, tag)
			if decoded == "GPSInfo":
				for t in value:
					sub_decoded = ExifTags.GPSTAGS.get(t, t)
					data[sub_decoded] = value[t]
			else:
				data[decoded] = value

	# Parse latitude/longitude
	if 'GPSLatitude' in data:
		latRaw = data['GPSLatitude']
		lat = (-1 if data['GPSLatitudeRef'] == 'S' else 1) * (float(latRaw[0]) + float(latRaw[1]) / 60 + float(latRaw[2]) / 3600)

		lonRaw = data['GPSLongitude']
		lon = (-1 if data['GPSLongitudeRef'] == 'W' else 1) * (float(lonRaw[0]) + float(lonRaw[1]) / 60 + float(lonRaw[2]) / 3600)

	# Parse date/time
	if 'GPSTimeStamp' in data:
		timeRaw = data['GPSTimeStamp']
		dateRaw = data['GPSDateStamp']
		msRaw = data['SubSecTimeOriginal'] if 'SubSecTimeOriginal' in data else "0"
		d = datetime.datetime.combine(
			datetime.date.fromisoformat(dateRaw.replace(":", "-")),
			datetime.time(
				int(timeRaw[0]),
				int(timeRaw[1]),
				int(timeRaw[2]),
				int(msRaw[:6].ljust(6, "0")),
				tzinfo=datetime.timezone.utc
			)
		)

	return {
		"lat": lat,
		"lon": lon,
		"ts": d.timestamp(),
		"heading": int(round(data['GPSImgDirection']))
	}


def generatePictureDerivates(fs, picture, sizing, outputFolder):
	"""Creates all derivated version of a picture (thumbnail, small, tiled)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Picture file
	sizing : dict
		Picture dimensions (width, height, cols, rows)
	outputFolder : str
		Path to output folder (relative to instance root)

	Returns
	-------
	bool
		True if worked
	"""

	# Thumbnail + fixed-with versions
	pictures.createThumbPicture(fs, picture, outputFolder + "/thumb.jpg")
	pictures.createSDPicture(fs, picture, outputFolder + "/sd.jpg")

	# Tiles
	tileFolder = outputFolder + "/tiles"
	fs.makedir(tileFolder)
	pictures.createTiledPicture(fs, picture, tileFolder, sizing["cols"], sizing["rows"])

	return True
