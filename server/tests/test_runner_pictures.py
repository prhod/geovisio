# ~ import time
# ~ import datetime
import os
import pytest
import psycopg
from psycopg.rows import dict_row
from PIL import Image, ImageChops, ImageStat
from fs import open_fs
from src import runner_pictures, create_app

FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)


def test_listSequencesToProcess_empty(tmp_path, dburl):
	# Create a sequence subfolder with a single picture
	seq1 = tmp_path / "seq1"
	seq1pic1 = seq1 / "1.jpg"
	seq1pic1.parent.mkdir()
	seq1pic1.touch()
	seq1pic1.write_text("Some text")

	# Create gvs_derivates folder
	gvs = tmp_path / "gvs_derivates"
	gvs.mkdir()

	# Call function
	with open_fs(str(tmp_path)) as fs:
		with psycopg.connect(dburl) as db:
			res = runner_pictures.listSequencesToProcess(fs, db)
			assert res == ["seq1"]

def test_listSequencesToProcess_existing(tmp_path, dburl):
	# Create a sequence subfolder with a single picture
	seq1 = tmp_path / "seq1"
	seq1pic1 = seq1 / "1.jpg"
	seq1pic1.parent.mkdir()
	seq1pic1.touch()
	seq1pic1.write_text("Some text")

	# Create gvs_derivates folder
	gvs = tmp_path / "gvs_derivates"
	gvs.mkdir()

	# Make it already initialized in DB
	with psycopg.connect(dburl) as db:
		db.execute("INSERT INTO sequences(folder_path, status) VALUES ('seq1', 'ready')")

		# Call function
		with open_fs(str(tmp_path)) as fs:
			res = runner_pictures.listSequencesToProcess(fs, db)
			assert res == []


@pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg'),
	os.path.join(FIXTURE_DIR, '2.jpg'),
	os.path.join(FIXTURE_DIR, '3.jpg'),
	os.path.join(FIXTURE_DIR, '4.jpg'),
	os.path.join(FIXTURE_DIR, '5.jpg')
)
def test_processSequence(datafiles, tmp_path, dburl):
	# Prepare folder hierarchy
	seqPath = tmp_path / "seq1"
	seqPath.mkdir()
	for i in range(1, 6):
		os.rename(tmp_path / (str(i)+".jpg"), seqPath / (str(i)+".jpg"))

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'ENABLE_BLUR': False, 'FS_URL': 'osfs://'+str(tmp_path) })

	# Run processing
	with app.app_context():
		with open_fs(str(tmp_path)) as fs:
			with psycopg.connect(dburl) as db:
				runner_pictures.processSequence(fs, db, "seq1")

				# Check results
				with psycopg.connect(dburl, row_factory=dict_row) as db2:
					# Sequence definition
					res0 = db2.execute("SELECT id, folder_path, status, metadata, ST_AsText(geom) AS geom FROM sequences").fetchall()[0]

					seqId = str(res0['id'])
					assert len(seqId) > 0
					assert res0['geom'] == "LINESTRING(1.919185441799137 49.00688961988304,1.919189623000528 49.0068986458004,1.919196360602742 49.00692625960235,1.919199780601944 49.00695484980094,1.919194019996227 49.00697341759938)"
					assert res0['status'] == "ready"
					assert res0['folder_path'] == "seq1"

					# Pictures
					res1 = db2.execute("SELECT id, ts, status FROM pictures ORDER BY ts").fetchall()

					assert len(res1) == 5
					assert len(str(res1[0]['id'])) > 0
					assert res1[0]['ts'].timestamp() == 1627550214.0
					assert res1[0]['status'] == 'ready'

					picIds = []
					for rec in res1:
						picIds.append(str(rec['id']))

					# Sequences + pictures
					with db2.cursor() as cursor:
						res2 = cursor.execute("SELECT pic_id FROM sequences_pictures WHERE seq_id = %s ORDER BY rank", [seqId]).fetchall()
						resPicIds = [ str(f['pic_id']) for f in res2 ]

						assert resPicIds == picIds

					# Check destination folder structure
					for picId in picIds:
						basePath = tmp_path / "gvs_derivates" / picId[0:2] / picId
						assert os.path.isdir(basePath)
						assert os.path.isdir(basePath / "tiles")
						assert os.path.isfile(basePath / "sd.jpg")
						assert os.path.isfile(basePath / "thumb.jpg")


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
def test_processPicture(datafiles, tmp_path, dburl):
	# Prepare folder hierarchy
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	seqPath = gvsPath / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'ENABLE_BLUR': False, 'FS_URL': 'osfs://'+str(gvsPath) })

	# Run processing
	with app.app_context():
		with open_fs(str(gvsPath)) as fs:
			with psycopg.connect(dburl) as db:
				runner_pictures.processPicture(fs, db, "seq1", "1.jpg", app.config)
				db.commit()

				# Check results in database
				with psycopg.connect(dburl, row_factory=dict_row) as db2:
					res = db2.execute("SELECT id, ts, heading, ST_X(geom) AS lon, ST_Y(geom) AS lat, status, metadata FROM pictures").fetchone()
					assert len(str(res['id'])) > 0
					assert res['ts'].timestamp() == 1627550214.0
					assert res['heading'] == 349
					assert res['lon'] == 1.9191854417991367
					assert res['lat'] == 49.00688961988304
					assert res['status'] == 'ready'
					assert res['metadata']['width'] == 5760
					assert res['metadata']['height'] == 2880
					assert res['metadata']['cols'] == 8
					assert res['metadata']['rows'] == 4

					# Check files on disk
					baseDerivPath = gvsPath / "gvs_derivates" / str(res['id'])[0:2] / str(res['id'])
					assert os.path.exists(baseDerivPath / "thumb.jpg")
					assert os.path.exists(baseDerivPath / "sd.jpg")
					assert os.path.exists(baseDerivPath / "tiles")


@pytest.mark.skipci
@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
def test_processPicture_withBlur(datafiles, tmp_path, dburl):
	# Prepare folder hierarchy
	gvsPath = tmp_path / "gvs"
	gvsPath.mkdir()
	seqPath = gvsPath / "seq1"
	seqPath.mkdir()
	os.rename(datafiles / "1.jpg", seqPath / "1.jpg")

	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'ENABLE_BLUR': 10, 'FS_URL': 'osfs://'+str(gvsPath) })

	# Run processing
	with app.app_context() as context:
		with open_fs(str(gvsPath)) as fs:
			with psycopg.connect(dburl) as db:
				from src import blur
				runner_pictures.processPicture(fs, db, "seq1", "1.jpg", app.config, blur.prepare())
				db.commit()

				# Check results in database
				with psycopg.connect(dburl, row_factory=dict_row) as db2:
					res = db2.execute("SELECT id FROM pictures").fetchone()
					assert len(str(res['id'])) > 0

					# Check files on disk
					baseDerivPath = gvsPath / "gvs_derivates" / str(res['id'])[0:2] / str(res['id'])
					picBlurPath = baseDerivPath / "blurred.jpg"
					picSdPath = baseDerivPath / "sd.jpg"
					assert os.path.exists(baseDerivPath / "thumb.jpg")
					assert os.path.exists(picSdPath)
					assert os.path.exists(picBlurPath)
					assert os.path.exists(baseDerivPath / "tiles")

					# Check derivates are based on blurred version
					picBlur = Image.open(picBlurPath)
					picSdBlur = picBlur.resize((2048, int(picBlur.size[1]*2048/picBlur.size[0])))
					diff = ImageChops.difference(
						picSdBlur.convert("RGB"),
						Image.open(picSdPath).convert("RGB")
					)
					stat = ImageStat.Stat(diff)
					diff_ratio = sum(stat.mean) / (len(stat.mean) * 255) * 100
					assert diff_ratio < 0.7


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
def test_readPictureMetadata(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/1.jpg"))
	assert result == { "lat": 49.00688961988304, "lon": 1.9191854417991367, "ts": 1627550214.0, "heading": 349 }


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'a1.jpg'))
def test_readPictureMetadata_negCoords(datafiles):
	result = runner_pictures.readPictureMetadata(Image.open(str(datafiles) + "/a1.jpg"))
	assert result == { "lat": 48.33756428166505, "lon": -1.9331088333333333, "ts": 1652453580.0, "heading": 32 }


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
def test_generatePictureDerivates(datafiles, tmp_path, dburl):
	srcPath = str(datafiles)

	destPath = tmp_path / "out"
	destPath.mkdir()

	with open_fs(str(tmp_path)) as fs:
		res = runner_pictures.generatePictureDerivates(
			fs,
			Image.open(srcPath+"/1.jpg"),
			{'cols': 8, 'rows': 4, 'width': 5760, 'height': 2880},
			"/out"
		)
		assert res is True

		# Check folder content
		assert sorted(fs.listdir("/out")) == [ "sd.jpg", "thumb.jpg", "tiles" ]
