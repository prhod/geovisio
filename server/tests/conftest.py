import pytest
import psycopg
import os
from fs import open_fs
import configparser

from src import create_app, runner_pictures


@pytest.fixture
def dburl():
	with open(os.path.join(os.path.dirname(__file__), '../config.py'), 'r') as f:
		config_string = '[main]\n' + f.read()
		config = configparser.ConfigParser()
		config.read_string(config_string)
		db = config['main']['TEST_DB_URL'].replace('"', '')

		sql = open(
			os.path.join(os.path.dirname(os.path.realpath(__file__)),
			'../src/db_setup.sql'
		), 'r')

		with psycopg.connect(db) as conn:
			with conn.cursor() as cursor:
				cursor.execute(sql.read())
				return db


@pytest.fixture
def app(dburl):
	app = create_app({ 'TESTING': True, 'DB_URL': dburl, 'SERVER_NAME': 'localhost:5000', 'ENABLE_BLUR': False })
	yield app


@pytest.fixture
def client(app):
	with app.test_client() as client:
		yield client


@pytest.fixture
def runner(app):
	return app.test_cli_runner()


# Code for having at least one sequence in tests
FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)

SEQ_IMG = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))

SEQ_IMGS = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg'),
	os.path.join(FIXTURE_DIR, '2.jpg'),
	os.path.join(FIXTURE_DIR, '3.jpg'),
	os.path.join(FIXTURE_DIR, '4.jpg'),
	os.path.join(FIXTURE_DIR, '5.jpg')
)

@pytest.fixture
def initSequence(tmp_path, dburl, app):
	seqPath = tmp_path / "seq1"
	seqPath.mkdir()

	def fct(datafiles):
		for i in range(1, 6):
			os.rename(datafiles / (str(i)+".jpg"), seqPath / (str(i)+".jpg"))

		with psycopg.connect(dburl) as db:
			with open_fs(str(tmp_path)) as fs:
				with app.app_context():
					runner_pictures.processSequence(fs, db, "seq1")

	return fct

@pytest.fixture
def initSequenceApp(tmp_path, dburl):
	seqPath = tmp_path / "seq1"
	seqPath.mkdir()

	def fct(datafiles):
		for i in range(1, 6):
			os.rename(datafiles / (str(i)+".jpg"), seqPath / (str(i)+".jpg"))

		with psycopg.connect(dburl) as db:
			with open_fs(str(tmp_path)) as fs:
				app = create_app({ 'TESTING': True, 'ENABLE_BLUR': False, 'DB_URL': dburl, 'SERVER_NAME': 'localhost:5000', 'FS_URL': 'osfs://'+str(datafiles) })
				with app.app_context():
					runner_pictures.processSequence(fs, db, "seq1")
					with app.test_client() as client:
						return client

	return fct
