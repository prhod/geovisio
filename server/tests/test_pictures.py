from flask import json
import os
import pytest
import psycopg
import io
import re
import math
from fs import open_fs
from PIL import Image, ImageChops, ImageStat
from . import conftest
from src import create_app, pictures


FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)


def test_getDerivatesPath(app):
	with app.app_context():
		assert pictures.getDerivatesPath("4366dddb-8a71-4f6e-a3d4-cb6b545476bb") == "/gvs_derivates/43/4366dddb-8a71-4f6e-a3d4-cb6b545476bb"


@conftest.SEQ_IMGS
def test_getPictureHD(datafiles, initSequenceApp, dburl):
	client = initSequenceApp(datafiles)

	# Retrieve loaded sequence metadata
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			picId = cursor.execute("SELECT id FROM pictures LIMIT 1").fetchone()[0]

			assert len(str(picId)) > 0

			# Call API
			response = client.get('/api/pictures/'+str(picId)+'/hd.jpg')

			assert response.status_code == 200
			assert response.content_type == "image/jpeg"

			# Call API on unexisting picture
			response = client.get('/api/pictures/00'+str(picId)[2:]+'/hd.jpg')
			assert response.status_code == 404

			# Call API on hidden picture
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picId])
			conn.commit()
			response = client.get('/api/pictures/'+str(picId)+'/hd.jpg')
			assert response.status_code == 403


@pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg')
)
def test_createSDPicture(datafiles, tmp_path):
	picture = Image.open(str(datafiles / '1.jpg'))
	destPath = str(tmp_path)

	# Generate file
	with open_fs(destPath) as fs:
		res = pictures.createSDPicture(fs, picture, '/sd.jpg')
		assert res is True

		# Check result file
		resImg = Image.open(destPath + '/sd.jpg')
		w, h = resImg.size
		assert w == 2048


@conftest.SEQ_IMGS
def test_getPictureSD(datafiles, initSequenceApp, dburl):
	client = initSequenceApp(datafiles)

	# Retrieve loaded sequence metadata
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			picId = cursor.execute("SELECT id FROM pictures LIMIT 1").fetchone()[0]

			assert len(str(picId)) > 0

			# Call API
			response = client.get('/api/pictures/'+str(picId)+'/sd.jpg')

			assert response.status_code == 200
			assert response.content_type == "image/jpeg"

			img = Image.open(io.BytesIO(response.get_data()))
			w, h = img.size
			assert w == 2048

			# Call API on unexisting picture
			response = client.get('/api/pictures/00'+str(picId)[2:]+'/sd.jpg')
			assert response.status_code == 404

			# Call API on hidden picture
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picId])
			conn.commit()
			response = client.get('/api/pictures/'+str(picId)+'/sd.jpg')
			assert response.status_code == 403


@pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg')
)
def test_createThumbPicture(datafiles, tmp_path):
	picture = Image.open(str(datafiles / '1.jpg'))
	destPath = str(tmp_path)

	# Generate file
	with open_fs(destPath) as fs:
		res = pictures.createThumbPicture(fs, picture, '/thumb.jpg')
		assert res is True

		# Check result file
		resImg = Image.open(destPath + '/thumb.jpg')
		w, h = resImg.size
		assert w == 500
		assert h == 300


@conftest.SEQ_IMGS
def test_getPictureThumb(datafiles, initSequenceApp, dburl):
	client = initSequenceApp(datafiles)

	# Retrieve loaded sequence metadata
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			picId = cursor.execute("SELECT id FROM pictures LIMIT 1").fetchone()[0]

			assert len(str(picId)) > 0

			# Call API
			response = client.get('/api/pictures/'+str(picId)+'/thumb.jpg')

			assert response.status_code == 200
			assert response.content_type == "image/jpeg"

			img = Image.open(io.BytesIO(response.get_data()))
			w, h = img.size
			assert w == 500
			assert h == 300

			# Call API on unexisting picture
			response = client.get('/api/pictures/00'+str(picId)[2:]+'/thumb.jpg')
			assert response.status_code == 404

			# Call API on hidden picture
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picId])
			conn.commit()
			response = client.get('/api/pictures/'+str(picId)+'/thumb.jpg')
			assert response.status_code == 403


@pytest.mark.parametrize(('imgWidth', 'tileCols'), (
	(512, 4),
	(1024, 4),
	(2048, 4),
	(4096, 8),
	(5760, 8),
	(8192, 16),
	(32768, 64),
	(655536, 64)
))
def test_getTileSize(imgWidth, tileCols):
	res = pictures.getTileSize((imgWidth, imgWidth / 2))
	assert isinstance(res[0], int)
	assert isinstance(res[1], int)
	assert res[0] == tileCols
	assert res[1] == tileCols / 2
	assert tileCols in [4,8,16,32,64]


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
def test_getPictureSizing(datafiles):
	res = pictures.getPictureSizing(Image.open(str(datafiles / '1.jpg')))
	assert res['cols'] == 8
	assert res['rows'] == 4
	assert res['width'] == 5760
	assert res['height'] == 2880


@pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
def test_createTiledPicture(datafiles, tmp_path):
	picture = Image.open(str(datafiles / '1.jpg'))
	destPath = str(tmp_path)
	cols = 4
	rows = 2

	# Generate tiles
	with open_fs(destPath) as fs:
		res = pictures.createTiledPicture(fs, picture, "/", cols, rows)
		assert res is True

		# Check every single file
		origImgSize = picture.size
		colWidth = math.floor(origImgSize[0] / cols)
		rowHeight = math.floor(origImgSize[1] / rows)

		for col in range(cols):
			for row in range(rows):
				tilePath = destPath + "/" + str(col) + "_" + str(row) + ".jpg"
				assert os.path.isfile(tilePath)
				tile = Image.open(tilePath)
				assert tile.size == (colWidth, rowHeight)

				origImgTile = picture.crop((colWidth * col, rowHeight * row, colWidth * (col+1), rowHeight * (row+1)))

				assert tile.height == origImgTile.height and tile.width == origImgTile.width

				if tile.mode == origImgTile.mode == "RGBA":
					img1_alphas = [pixel[3] for pixel in tile.getdata()]
					img2_alphas = [pixel[3] for pixel in origImgTile.getdata()]
					assert img1_alphas == img2_alphas

				diff = ImageChops.difference(tile.convert("RGB"), origImgTile.convert("RGB"))
				stat = ImageStat.Stat(diff)
				diff_ratio = sum(stat.mean) / (len(stat.mean) * 255) * 100
				assert diff_ratio <= 1 # Less than 1% difference


def test_getPictureTiledEmpty(tmp_path, client):
	# Call API on unexisting picture
	response = client.get('/api/pictures/00000000-0000-0000-0000-000000000000/tiled/0_0.jpg')
	assert response.status_code == 404


@pytest.mark.parametrize(('col', 'row', 'httpCode', 'picStatus'), (
	(0, 0, 200, 'ready'),
	(7, 3, 200, 'ready'),
	(8, 4, 404, 'ready'),
	(-1, -1, 404, 'ready'),
	(0, 0, 403, 'hidden'),
))
@conftest.SEQ_IMGS
def test_getPictureTiled(datafiles, initSequenceApp, dburl, col, row, httpCode, picStatus):
	client = initSequenceApp(datafiles)

	# Retrieve loaded sequence metadata
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			picId = cursor.execute("SELECT id FROM pictures LIMIT 1").fetchone()[0]

			assert len(str(picId)) > 0

			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			assert len(str(seqId)) > 0

			if picStatus != 'ready':
				cursor.execute("UPDATE pictures SET status = %s WHERE id = %s", (picStatus, picId))
				conn.commit()

			# Call API
			response = client.get('/api/pictures/'+str(picId)+'/tiled/'+str(col)+'_'+str(row)+'.jpg')

			assert response.status_code == httpCode

			if httpCode == 200:
				assert response.content_type == "image/jpeg"

				diskImg = Image.open(str(datafiles) +"/gvs_derivates/"+ str(picId)[0:2] + "/" + str(picId) +"/tiles/"+ str(col)+'_'+str(row)+'.jpg')
				apiImg = Image.open(io.BytesIO(response.get_data()))

				assert not ImageChops.difference(diskImg, apiImg).getbbox()
